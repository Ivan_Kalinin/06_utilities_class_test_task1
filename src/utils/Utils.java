package utils;

import java.math.BigInteger;
import java.util.regex.Pattern;

import enums.StringState;

public class Utils {
  public static String concatenateWords(String leftString, String rightString) {
    String regex = "[\\p{IsLatin}+\\s]*";

    if (leftString == null) {
      if (rightString == null)
        return null;
      else if (rightString.isEmpty())
        return StringState.EMPTY.toString();
      else if (Pattern.matches(regex, rightString))
        return rightString;
      else
        return StringState.NON_LATIN.toString();
    } else if (leftString.isEmpty()) {
      if (rightString == null)
        return StringState.EMPTY.toString();
      if (rightString.isEmpty())
        return StringState.EMPTY.toString();
      else if (Pattern.matches(regex, rightString))
        return rightString;
      else
        return StringState.NON_LATIN.toString();
    } else if (Pattern.matches(regex, leftString)) {
      if (rightString == null)
        return leftString;
      if (rightString.isEmpty())
        return leftString;
      else if (Pattern.matches(regex, rightString))
        return leftString + rightString;
      else
        return StringState.NON_LATIN.toString();
    } else {
      return StringState.NON_LATIN.toString();
    }
  }

  public static BigInteger factorial(int n) {
    BigInteger ret = BigInteger.ONE;
    for (int i = 1; i <= n; ++i)
      ret = ret.multiply(BigInteger.valueOf(i));
    return ret;
  }
}
