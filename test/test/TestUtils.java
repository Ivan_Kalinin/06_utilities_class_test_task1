package test;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.Random;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import enums.StringState;
import utils.Utils;

public class TestUtils {
  private static String leftString;
  private static String rightString;

  @BeforeClass
  public static void ini() {
    leftString = "hello";
    rightString = "world";
  }

  @Test
  @Ignore
  public void testComputeFactorial() {
    assertEquals("factorial value should equal to 1", BigInteger.ONE, Utils.factorial(0));
    assertEquals("factorial value should equal to 1", BigInteger.valueOf(1), Utils.factorial(1));
    assertEquals("factorial value should equal to 6", BigInteger.valueOf(6), Utils.factorial(3));
  }

  @Test(timeout = 500, expected = InterruptedException.class)
  public void testComputeFactorialWithTimeout() throws InterruptedException {
    assertEquals("factorial value should equal to 1", BigInteger.ONE, Utils.factorial(0));
    assertEquals("factorial value should equal to 1", BigInteger.valueOf(1), Utils.factorial(1));
    assertEquals("factorial value should equal to 3628800", BigInteger.valueOf(3628800),
        Utils.factorial(10));

    Random random = new Random();
    while (true) {
      Utils.factorial(random.nextInt());
    }

  }

  @Test
  public void testConcatenateGoodStrings() {
    // good string
    assertEquals("the method should to return concatenated string", leftString + rightString,
        Utils.concatenateWords(leftString, rightString));
  }

  @Test
  public void testConcatenateNullsAndEmptyStrings() {
    // nulls and empty strings
    assertEquals("the method should to return emptiness", StringState.EMPTY.toString(),
        Utils.concatenateWords("", ""));
    assertEquals("the method should to return rightString", StringState.EMPTY.toString(),
        Utils.concatenateWords(null, ""));
    assertEquals("the method should to return leftString", StringState.EMPTY.toString(),
        Utils.concatenateWords("", null));
    assertEquals("the method should to return null", null, Utils.concatenateWords(null, null));
  }

  @Test
  public void testConcatenateGoodAndEmptyStrings() {
    // good and empty stringss
    assertEquals("the method should to return rightString", rightString,
        Utils.concatenateWords("", rightString));
    assertEquals("the method should to return leftString", leftString,
        Utils.concatenateWords(leftString, ""));
  }

  @Test
  public void testConcatenateNullAndGoodStrings() {
    // nulls and good string
    assertEquals("the method should to return rightString", rightString,
        Utils.concatenateWords(null, rightString));
    assertEquals("the method should to return leftString", leftString,
        Utils.concatenateWords(leftString, null));
  }

  @Test
  public void testConcatenateNonLatinAndGoodStrings() {
    // non-latin and good string
    assertEquals("the method should to non-latin", StringState.NON_LATIN.toString(),
        Utils.concatenateWords("������ ", "���"));
    assertEquals("the method should to non-latin", StringState.NON_LATIN.toString(),
        Utils.concatenateWords(leftString, "���"));
    assertEquals("the method should to non-latin", StringState.NON_LATIN.toString(),
        Utils.concatenateWords("������ ", rightString));
  }

  @Test
  public void testConcatenateNonLatinAndNullStrings() {
    // non-latin and nulls string
    assertEquals("the method should to latin", StringState.NON_LATIN.toString(),
        Utils.concatenateWords(null, "���"));
    assertEquals("the method should to latin", StringState.NON_LATIN.toString(),
        Utils.concatenateWords("������ ", null));
  }

  @Test
  public void testConcatenateNonLatinAndEmptyStrings() {
    // non-latin and empty string
    assertEquals("the method should to non-latin", StringState.NON_LATIN.toString(),
        Utils.concatenateWords("", "���"));
    assertEquals("the method should to non-latin", StringState.NON_LATIN.toString(),
        Utils.concatenateWords("������ ", ""));
  }
}
